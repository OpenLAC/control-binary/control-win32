import sys
import os
from shutil import copy
import subprocess
import time
import numpy as np
import win32api

#==============================================================================
def getFileProperties(fname):
#==============================================================================
    """
    Read all properties of the given file return them as a dictionary.
    """
    propNames = ('Comments', 'InternalName', 'ProductName',
        'CompanyName', 'LegalCopyright', 'ProductVersion',
        'FileDescription', 'LegalTrademarks', 'PrivateBuild',
        'FileVersion', 'OriginalFilename', 'SpecialBuild')

    props = {'FixedFileInfo': None, 'StringFileInfo': None, 'FileVersion': None}

    try:
        # backslash as parm returns dictionary of numeric info corresponding to VS_FIXEDFILEINFO struc
        fixedInfo = win32api.GetFileVersionInfo(fname, '\\')
        props['FixedFileInfo'] = fixedInfo
        props['FileVersion'] = "%d.%d.%d.%d" % (fixedInfo['FileVersionMS'] / 65536,
                fixedInfo['FileVersionMS'] % 65536, fixedInfo['FileVersionLS'] / 65536,
                fixedInfo['FileVersionLS'] % 65536)

        # \VarFileInfo\Translation returns list of available (language, codepage)
        # pairs that can be used to retreive string info. We are using only the first pair.
        lang, codepage = win32api.GetFileVersionInfo(fname, '\\VarFileInfo\\Translation')[0]

        # any other must be of the form \StringfileInfo\%04X%04X\parm_name, middle
        # two are language/codepage pair returned from above

        strInfo = {}
        for propName in propNames:
            strInfoPath = u'\\StringFileInfo\\%04X%04X\\%s' % (lang, codepage, propName)
            ## print str_info
            strInfo[propName] = win32api.GetFileVersionInfo(fname, strInfoPath)

        props['StringFileInfo'] = strInfo
    except:
        pass

    return props


def run_git_cmd(cmd, git_repo_path=None):
    git_repo_path = git_repo_path or os.getcwd()
    try:
        process = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True,
                                   cwd=os.path.abspath(git_repo_path))
        stdout,stderr = process.communicate()
        if process.returncode != 0:
            raise EnvironmentError("%s\n%s"%(stdout, stderr))
        return stdout.strip()

    except EnvironmentError as e:
        raise e
        raise Warning("unable to run git")
    
def update_contents():
    h2bin_path = os.path.dirname(__file__) + '/'
    for fn in os.listdir(h2bin_path):
        #if getFileProperties(fn)['StringFileInfo']:
        if getFileProperties(fn)['StringFileInfo'] and getFileProperties(fn)['StringFileInfo']['ProductVersion']:
            version = getFileProperties(fn)['StringFileInfo']['ProductVersion']
        else:
            version = getFileProperties(fn)['FileVersion']
        if version is None:
            continue
     
        s = "%-30s\t%s\n"%(os.path.basename(fn), version)
        print (s)
        with open(h2bin_path + "contents.txt") as fid:
            lines = fid.readlines()
        for i, l in enumerate(lines):
            if l.lower().startswith(os.path.basename(fn).lower()):
                lines[i] = s
                break
        else:
            lines.append(s)
        with open(h2bin_path + "contents.txt", 'w') as fid:
            fid.writelines(lines)

if __name__ == '__main__':
    if len(sys.argv)>1:
        fn = sys.argv[1]
    
        h2bin_path = os.path.dirname(__file__) + '/'
        if os.path.isfile(fn):
            copy(fn, h2bin_path)
            if len(sys.argv) == 3:
                if sys.argv[2] == 'master-test':
                    pass
            else:
                if getFileProperties(fn)['StringFileInfo'] and getFileProperties(fn)['StringFileInfo']['ProductVersion']:
                    version = getFileProperties(fn)['StringFileInfo']['ProductVersion']
                else:
                    version = getFileProperties(fn)['FileVersion'] or time.ctime(os.path.getctime(fn)) 
                s = "%-30s\t%s\n"%(os.path.basename(fn), version)
                with open(h2bin_path + "contents.txt") as fid:
                    lines = fid.readlines()
                for i, l in enumerate(lines):
                    if l.lower().startswith(os.path.basename(fn).lower()):
                        lines[i] = s
                        break
                else:
                    lines.append(s)
                with open(h2bin_path + "contents.txt", 'w') as fid:
                    fid.writelines(lines)
                print ("Add and commit: %s"%s.strip())
                
                module_git_path = os.path.dirname(fn)
                module_tag = run_git_cmd("git tag --sort=-creatordate", module_git_path).split("\n")[0].strip()
                current_bin_tag = run_git_cmd("git tag --sort=-creatordate", h2bin_path).split("\n")[0].strip()
                print('current_bin_tag: ', current_bin_tag)
                module = os.path.basename(fn)
                new_bin_tag = ".".join(map(str,np.array(current_bin_tag.split(".")).astype(np.int) + [0,0,1]))
                message = run_git_cmd("git tag -n99 %s"%module_tag, module_git_path).replace(module_tag, "").strip()
                date = run_git_cmd('git log -1 --format=%%ai --tags %s'%module_tag, module_git_path)[:10].strip()
                name = run_git_cmd('git log -1 --format="%%an" --tags %s'%module_tag, module_git_path).strip()
                
                
                with open(h2bin_path + 'changelog.txt','a') as fid:
                    info = "  ".join(["%-12s"%new_bin_tag, date, "%s(%s)"%(module, module_tag)])
                    message = ("\n"+" "*60).join([l.strip() for l in message.split("\n")])
                    fid.write("%-60s%s\n"%(info[:58], message))
                
                run_git_cmd('git add "%s"'%os.path.basename(fn), h2bin_path)
                run_git_cmd('git commit -a -m "%s"'%s, h2bin_path)
                run_git_cmd('git tag "%s"'%new_bin_tag, h2bin_path)
        elif fn=='push':
            run_git_cmd('git push', h2bin_path)
            run_git_cmd('git push --tags', h2bin_path)
            print ("Done")
        else:
            raise FileNotFoundError("'%s' not found" % os.path.abspath(fn))
    else:
        raise Exception("Usage: \n>> python update_module.py <dll_name>")
